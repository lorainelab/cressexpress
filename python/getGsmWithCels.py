#!/usr/bin/env python


import os,sys

# non-standard libraries
import requests
from lxml import etree as etree


# create ReST-style query URL to retrieve GSMs with CEL file data
def getQuery(platform='GPL198'):
      url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term="+platform+"[ACCN]+AND+gsm[ETYP]+AND+cel[suppFile]&retmax=100000&usehistory=y"
      return url

# run the query, return DOM object
def runQuery(url=None):
    headers={'accept':'application/xml'}
    response = requests.get(url, headers=headers, stream=True)
    doc = etree.parse(response.raw)
    return doc


if __name__ == '__main__':
      platform=sys.argv[1]

