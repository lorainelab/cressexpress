#This script fetches list of all the GSE ids belonging to GPL 198. It then fetches a document summary of every GSE id and writes it to a file.

#URL to fetch GSE ids for GPL 198 is :
# http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=GPL198[ACCN]+AND+gse[ETYP]&retmax=10000&usehistory=y

#The script takes one argument which is :
#Output path for xml files: The xml files will be written to this directory. eg: "/Users/lorainelab/Nikhil_Dahake/GSE_XMLs/"


import sys,urllib
import requests
from lxml import etree as ET
import re
import subprocess
import os
import glob
from xml.dom.minidom import parseString

OutputPath=""
def main(URL=None):
    getData(URL=URL)
  

def getData(URL=None):
    
        headers={'accept':'application/xml'}
        response = requests.get(URL, headers=headers, stream=True)
        print 'Response Received'
        doc = ET.parse(response.raw)
        itemlist = doc.xpath("/eSearchResult/IdList/Id")# Extracting all the GSM IDs from the XML
        print "Total number of GSE IDs: "+str(len(itemlist))
        print "Please Wait..."
        #f = open(OutputFile,'w')
        
        id=""
        for s in itemlist:
            headers={'accept':'application/xml'}
            xml_info_url="http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gds&version=2.0&id="+str(s.text)
            #print xml_info_url
            response = requests.get(xml_info_url, headers=headers, stream=True)
            doc = response.content
            dom = parseString(doc)
            GSE_ID = dom.getElementsByTagName('Accession')[0].toxml()
            GSE_ID=GSE_ID.replace('<Accession>','').replace('</Accession>','')
            GSE_ID=GSE_ID+'.xml'
            #print GSM_ID
            #print doc
            #Check if GSM is present in our directory
                    
            try:
              
                
               f = open(OutputPath+GSE_ID,'w')
               f.write(doc)
               f.close
                             
                          
            except Exception as e:
                print e
                print "Something went wrong in the script!!"  
                                         
        print "All the XMLs have been written to : "+OutputPath
        
         
    
if __name__ == '__main__':
      global OutputPath
      OutputPath=sys.argv[1]
      
      url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=GPL198[ACCN]+AND+gse[ETYP]&retmax=10000&usehistory=y"
      main(url)