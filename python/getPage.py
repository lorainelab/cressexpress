#!/usr/bin/env python 

import sys,urllib

def main(url=None):
    txt = getData(url=url)
    print txt

def getData(url=None):
    filestream=urllib.urlopen(url)
    txt=filestream.read()
    return txt

if __name__ == '__main__':
    url=sys.argv[1]
    nameofprogram=sys.argv[0]
    sys.stderr.write("%s I got an argument: %s\n"%(nameofprogram,url))
    main(url)
    
