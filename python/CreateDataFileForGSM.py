# This script will create a data file which can be used to load data into mysql tables for coexpression 2.0
#This script takes a single argument at runtime. The argument specifies the path to the xml files from which the contents for the data file need to be picked up.
#eg. /Users/lorainelab/Nikhil_Dahake/GSM_XMLs/

import sys,urllib
import requests
from lxml import etree as ET
import re
import subprocess
import os
import glob
from xml.dom.minidom import parseString

XML_File_path=""
def main():
    createDataFile()
  

def createDataFile():
    
        DataFile="Data.txt" # This is the data file we are creating. Contents from xml will be read and written to this
        print "Please Wait..."
        arr= glob.glob(XML_File_path+"*.xml") #arr will store the names of all the xml files which need to be read 
        for xmlFile in arr:
            
           f = open(xmlFile, "r")
           doc = f.read()
           dom = parseString(doc)
            			
	   GSM_ID = dom.getElementsByTagName('Accession')[0].toxml();
           GSM_ID = GSM_ID.replace('<Accession>','').replace('</Accession>','') #GSM_ID stores the accession number
           Title = dom.getElementsByTagName('title')[0].toxml()
           Title = Title.replace('<title>','').replace('</title>','')
           Summary = dom.getElementsByTagName('summary')[0].toxml()
           Summary = Summary.replace('<summary>','').replace('</summary>','')
	   PDAT = dom.getElementsByTagName('PDAT')[0].toxml()
	   PDAT = PDAT.replace('<PDAT>','').replace('</PDAT>','')
	   GSE = dom.getElementsByTagName('GSE')[0].toxml()
	   GSE = GSE.replace('<GSE>','').replace('</GSE>','')

	   GDS = dom.getElementsByTagName('GDS')[0].toxml()
	   GDS = GDS.replace('<GDS>','').replace('</GDS>','')
	   GDS = GDS.replace('<GDS/>','');
    	   GDS = GDS.strip();

	   GSM_ID = GSM_ID.encode(sys.stdout.encoding, 'replace') #To handle some characters which cannot be represented correctly by unicode
           Title = Title.encode(sys.stdout.encoding, 'replace')
           Summary = Summary.encode(sys.stdout.encoding, 'replace')
	   PDAT = PDAT.encode(sys.stdout.encoding, 'replace')
	   GSE = GSE.encode(sys.stdout.encoding, 'replace')
	   GDS = GDS.encode(sys.stdout.encoding, 'replace')
            
            
                    
           try:
               f = open(XML_File_path+DataFile,'a')
               GDS=GDS.strip();
               if GDS!= "":
                  
                  if ";" in GDS:
                      GDS_List = GDS.split(";")

                      for GDS in GDS_List:
                          f.write(GSM_ID+"\t"+"GEO"+"\t"+"GDS"+"\t"+GDS+"\n")
                  else :
                        f.write(GSM_ID+"\t"+"GEO"+"\t"+"GDS"+"\t"+GDS+"\n")
               f.write(GSM_ID+"\t"+"GEO"+"\t"+"Title"+"\t"+Title+"\n")
               f.write(GSM_ID+"\t"+"GEO"+"\t"+"Accession"+"\t"+GSM_ID+"\n") #Accession is same GSM_ID
	       f.write(GSM_ID+"\t"+"GEO"+"\t"+"Summary"+"\t"+Summary+"\n")
	       f.write(GSM_ID+"\t"+"GEO"+"\t"+"PDAT"+"\t"+PDAT+"\n")
               if ";" in GSE:
                  GSE_List = GSE.split(";"); #one GSM id can be a part of several other GSEs
                  for GSE in GSE_List:
                       f.write(GSM_ID+"\t"+"GEO"+"\t"+"GSE"+"\t"+"GSE"+GSE+"\n")#just appending the text 'GSE'to the GSE number
               else :
                          f.write(GSM_ID+"\t"+"GEO"+"\t"+"GSE"+"\t"+"GSE"+GSE+"\n")
	       f.close
                          
           except Exception as e:
                print e
                print "Something went wrong in the script!!"  
                
                                         
        print "The path for the data file is : "+XML_File_path+DataFile
        
         
    
if __name__ == '__main__':
      global XML_File_path
      XML_File_path=sys.argv[1]
            
      main()
