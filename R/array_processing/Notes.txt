Notes:

Nikhil downloaded CEL files from GEO and I transferred them onto the cluster. 
I then attempted to process them using Bioconductor. 

To start latest R, run alias R3 - this will launch a Loraine lab installation of R v3.

Before we can load microarray dta into the database, we have to check that
the CEL files can be read and processed.

For this, see the functions in the file named testCelFiles.R in this same
directory. 

The function readMappingFile reads a mapping file that maps GSM ids onto
a CEL file name. This was a file that Nikhil provided. It returns a data
frame with two columsn - cel and gsm.

Pass this data frame to testCels, which will attempt to read each of the CEL
files listed in the data frame. It returns a vector that contains TRUE or
FALSE values for each GSM file in the data frame. 

These functions are invoked by chekcFiles, which will write out a a
file "tested.tsv" which reports the result of testing each file.

Note that most of the files were readable, but some were not. This
means we can't use all the GSM arrays.

There were 9032 rows in tested.tsv.
There were 8997 unique GSM ids in tested.tsv.
There were 8975 rows in test.tsv where the CEL file was readable.




