
library(affy)


d=readMappingFile=function(fname='data/gsm2cel.tsv') {
  d=read.delim(fname,header=F,sep='\t')
  names(d)=c('gsm','cel')
  d$cel=as.character(d$cel)
  d$gsm=as.character(d$gsm)
}

getNames = function(d,celdir='CEL') {
  fnames=sapply(d$cel,function(x)file.path(celdir,x))
  return(fnames)
}

testCels = function(d,celdir='CEL') {
  fnames=getNames(d,celdir=celdir)
  v = sapply(fnames,testCel)
  return(v)
}

makePsList=function() {
  dr='~/Dropbox/AbioticStressMicroarray/Data/Raw/CtrlAndTreatment/'
  filenames=sapply(dir(dr,pattern='*.CEL.gz')[1],
                   function(x){paste0(dr,x)},
                   USE.NAMES=F)
  #return(filenames)
  read.affybatch(filenames=filenames)
}

testCel = function(fname) {
  if (!file.exists(fname)) {return(F)}
  result = tryCatch(
            read.affybatch(filenames=c(fname)),
            error=function(cond) {
              return(NA)
          })
  return(!is.na(result))
}

checkFiles = function(f='data/tested.tsv') {
  d=readMappingFile('data/gsm2cel.tsv')
  v=testCels(d,celdir='CEL')
  d2=cbind(d,test=v)
  write.table(d2,f,row.names=F,quote=F,sep='\t')
}

readCheckedFiles = function(fname='tested.tsv') {
  d=read.delim(fname,header=T,sep='\t',)
  d$cel=as.character(d$cel)
  d$gsm=as.character(d$gsm)
  d=subset(d,d$test==T)
  o=order(d$gsm)
  d=d[o,]
  d=subset(d,!duplicated(d$gsm))
  d
}


# d is output of readCheckedFiles
procCels=function(d,celdir='CEL') {
  exprs=ReadAffy(filenames=d$cel,sampleNames=d$gsm,celfile.path=celdir)
  return(exprs)
}

# d=readCheckedFiles()
# can we process the first half?
# N=floor(nrow(d)/2)
# no
# can we process the first third? (2980)
# N=floor(nrow(d)/3)
# yes
# can we process the first 3500?
# N=3500
# can we process the first 4500?
# no
# N=4500
# can we process the first 4000?
# N=4000
# exprs=procCels(d[1:N,],celdir='CEL')
# no
