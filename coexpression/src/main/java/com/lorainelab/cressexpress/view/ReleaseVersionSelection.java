/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view;

import com.lorainelab.cressexpress.controller.ExprColumnController;
import com.lorainelab.cressexpress.controller.ReleaseVersionController;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class ReleaseVersionSelection implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(ReleaseVersionSelection.class);

    private ReleaseVersion selectedVersion;

    @Inject
    private ReleaseVersionController versionController;

    @Inject
    private ExprColumnController exprColController;

    public ReleaseVersionSelection() {
    }

    @PostConstruct
    public void init() {
        selectedVersion = getSelectedVersion();
    }

    //in the future we may have a preceding step to allow the platform to be selected
    public List<ReleaseVersion> getAvailableVersions() {
        List<ReleaseVersion> results = versionController.getVersionByPlatformId(1);
        return results;
    }

    public String getVersionLabel(ReleaseVersion version) {
        Integer count = exprColController.getCountByReleaseVersionId(version.getId());
        String releaseDetails = "Version " + version.getName() + " (" + count + "  arrays - RMA processing)";
        return releaseDetails;
    }

    public ReleaseVersion getSelectedVersion() {
        if (selectedVersion == null) {
            selectedVersion = versionController.getDefaultVersion();
        }
        return selectedVersion;
    }

    public void setSelectedVersion(ReleaseVersion selectedVersion) {
        this.selectedVersion = selectedVersion;
    }

    public void setVersion(ReleaseVersion selectedVersion) {
        this.selectedVersion = selectedVersion;
    }

    public void navigateNext() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/step2.xhtml");
    }

    public void navigatePrev() {
        //do nothing since this is for step 1
    }

}
