/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view.validators;

import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dcnorris
 */
@FacesValidator("jobValidator")
public class JobValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Integer selectionCount = (Integer) component.getAttributes().get("microArraySelectionCount");

        if (selectionCount < 1) {
            throw new ValidatorException(new FacesMessage(
                    "You must select at least 1 microarray."));
        }

        Set<String> validProbesets = (Set<String>) component.getAttributes().get("validProbesets");
        if (validProbesets == null || validProbesets.isEmpty()) {
            throw new ValidatorException(new FacesMessage(
                    "You must select at least 1 probeset."));
        }
        
        ReleaseVersion selectedVersion = (ReleaseVersion) component.getAttributes().get("selectedVersion");
        if (selectedVersion == null) {
            throw new ValidatorException(new FacesMessage(
                    "You must select a release version in Step 1."));
        }
        
        String email = (String) component.getAttributes().get("email");
        if (StringUtils.isBlank(email)){
         throw new ValidatorException(new FacesMessage(
                    "You must provide a valid email"));
        }
        

    }
}
