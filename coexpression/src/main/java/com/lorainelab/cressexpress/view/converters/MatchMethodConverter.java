/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view.converters;

import com.lorainelab.cressexpress.controller.MatchMethodController;
import com.lorainelab.cressexpress.model.MatchMethod;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author dcnorris
 */
@FacesConverter(value = "matchMethodConverter")
public class MatchMethodConverter implements Converter {

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        // Convert unique String representation of ProjectDetail back to ProjectDetail object.
        MatchMethod matchMethod = (MatchMethod) value;
        String idAsString = String.valueOf(matchMethod.getId());
        return idAsString;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        // Convert ProjectDetail to its unique String representation.
        MatchMethodController matchMethodController = null;
        try {
            InitialContext ic = new InitialContext();
            matchMethodController = (MatchMethodController) ic.lookup("java:global/coexpression-1.0/MatchMethodController");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        Integer id = Integer.parseInt(value);
        MatchMethod matchMethod = matchMethodController.findMatchMethod(id);
        return matchMethod;
    }

}
