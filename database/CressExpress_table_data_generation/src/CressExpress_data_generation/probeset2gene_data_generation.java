/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package CressExpress_data_generation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author lorainelab
 */
public class probeset2gene_data_generation {

    public static void main(String[] args) {
         String file=args[0];
         String outputFile = args[1];


        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String user = "ndahake";
            String pass = "itfungtopove1!";

            String URL = "jdbc:mysql://localhost:6963/coexpression2?autoReconnect=true";
            
            Connection conn = DriverManager.getConnection(URL, user, pass);

            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            FileWriter fw = new FileWriter(outputFile);
            BufferedWriter bw = new BufferedWriter(fw);


            String str = "";
            String probeset_id_SQL = "SELECT id FROM probeset WHERE name = ";
            String gene_id_SQL = "SELECT id FROM gene WHERE name = ";
            Statement stmnt = conn.createStatement();


            ResultSet res;
            br.readLine();//ignoring the header

            while ((str = br.readLine()) != null) {


                String arr[] = str.split("\t");
                String probeset_name = arr[0].trim();
                String agi_name = arr[3].trim();

                String probeset_id=null;
                String gene_id = null;
                res = stmnt.executeQuery(probeset_id_SQL + "'"+probeset_name+"'");

                while (res.next()) {
                      probeset_id = res.getInt("id") + "";

                }

                res = stmnt.executeQuery(gene_id_SQL + "'"+agi_name+"'");

                while (res.next()) {
                      gene_id = res.getInt("id") + "";

                }


                bw.write(probeset_id + "\t" +gene_id+ "\t" +"2" +"\t"+probeset_name+"\t"+agi_name+"\n");


            }

            bw.flush();
            bw.close();
            conn.close();
            stmnt.close();
            
            System.out.println("The mapping has been written to: " + outputFile);


        } catch (Exception e) {

            System.out.println(e.toString());
        }
    }

}
