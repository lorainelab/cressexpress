## CressExpress

This repository contains a Web-based application called CressExpress and its code for its associated database.

The Web application lets users find genes based on how closely their expression pattern across many thousands of experiments matches user-supplied query genes.

The database is designed to support power users developing new algorithsm and tools for mining expression data.

## Build the Web app
1. Clone repository 
2. Install redis Server on machine: refer https://redis.io/topics/quickstart 
3. Open terminal/command prompt from the root of the cloned repository
4. Execute following command: `mvn clean install`
5. Wait till success message
6. Go to /coexpression/target/ for coexpression-1.0.war file 

## Deploy the Web app

Follow instructions at [https://bitbucket.org/lorainelab/cressexpress-docker].


