package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.facade.MicroarrayAttributeFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class MicroarrayAttributeController implements Serializable {

    @EJB
    private MicroarrayAttributeFacade ejbFacade;

    public MicroarrayAttributeController() {
    }

}
