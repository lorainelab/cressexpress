package com.lorainelab.cressexpress.controller;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.lorainelab.cressexpress.model.Probeset2geneMap;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

@Stateless
@LocalBean
public class Probeset2geneMapController implements Serializable {

    @EJB
    private com.lorainelab.cressexpress.facade.Probeset2geneMapFacade ejbFacade;

    public Probeset2geneMapController() {
    }

//    public Table<String, Integer, String> getAgiToProbesetMap() {
//        //Select probeset2gene.gene_name, probeset2gene.match_method_id, probeset2gene.probeset_name from coexpression2.probeset2gene
//        Table<String, Integer, String> agiToProbesetIndex = HashBasedTable.create();
//        String query = "select distinct(p.name) as ps, g.name as AGI,p2m.matchMethod from probeset p, gene g, probeset2geneMap p2m where p.id = p2m.probeset and g.id = p2m.gene";
//        Session session = (Session) ejbFacade.getEntityManager().getDelegate();
//        final org.hibernate.Query releaseInfoQuery = session.createSQLQuery(query);
//        releaseInfoQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
//        List<Map<String, Object>> resultList = (List<Map<String, Object>>) releaseInfoQuery.list();
//        for (Map<String, Object> map : resultList) {
//            agiToProbesetIndex.put((String) map.get("ps"), (Integer) map.get("matchMethod"), (String) map.get("AGI"));
//        }
//        return agiToProbesetIndex;
//    }
    public SetMultimap<String, String> getAgiToProbesetMap() {
        //Select probeset2gene.gene_name, probeset2gene.match_method_id, probeset2gene.probeset_name from coexpression2.probeset2gene
        SetMultimap<String, String> agiToProbesetIndex = HashMultimap.create();
        String query = "select p.name as ps, g.name as AGI from probeset p, gene g, probeset2geneMap p2m where p.id = p2m.probeset and g.id = p2m.gene";
        Session session = (Session) ejbFacade.getEntityManager().getDelegate();
        final org.hibernate.Query releaseInfoQuery = session.createSQLQuery(query);
        releaseInfoQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<Map<String, Object>> resultList = (List<Map<String, Object>>) releaseInfoQuery.list();
        for (Map<String, Object> map : resultList) {
            agiToProbesetIndex.put((String) map.get("AGI"), (String) map.get("ps"));
        }
        return agiToProbesetIndex;
    }

    public List<Probeset2geneMap> findProbeset2geneEntities() {
        return findProbeset2geneEntities(true, -1, -1);
    }

    public List<Probeset2geneMap> findProbeset2geneEntities(int maxResults, int firstResult) {
        return findProbeset2geneEntities(false, maxResults, firstResult);
    }

    private List<Probeset2geneMap> findProbeset2geneEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Probeset2geneMap.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public Probeset2geneMap findProbeset2gene(Integer id) {
        EntityManager em = ejbFacade.getEntityManager();
        return em.find(Probeset2geneMap.class, id);
    }

    public int getProbeset2geneCount() {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Probeset2geneMap> rt = cq.from(Probeset2geneMap.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
