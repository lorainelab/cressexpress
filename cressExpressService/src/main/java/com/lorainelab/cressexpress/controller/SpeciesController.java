package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.controller.exceptions.IllegalOrphanException;
import com.lorainelab.cressexpress.controller.exceptions.NonexistentEntityException;
import com.lorainelab.cressexpress.controller.exceptions.PreexistingEntityException;
import com.lorainelab.cressexpress.facade.SpeciesFacade;
import com.lorainelab.cressexpress.model.Gene;
import com.lorainelab.cressexpress.model.Species;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class SpeciesController implements Serializable {

    @EJB
    private SpeciesFacade ejbFacade;

    public SpeciesController() {
    }

    public EntityManager getEntityManager() {
        return ejbFacade.getEntityManager();
    }

    public void create(Species species) throws PreexistingEntityException, Exception {
        if (species.getGenes() == null) {
            species.setGenes(new HashSet<Gene>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Set<Gene> attachedGenes = new HashSet<Gene>();
            for (Gene genesGeneToAttach : species.getGenes()) {
                genesGeneToAttach = em.getReference(genesGeneToAttach.getClass(), genesGeneToAttach.getId());
                attachedGenes.add(genesGeneToAttach);
            }
            species.setGenes(attachedGenes);
            em.persist(species);
            for (Gene genesGene : species.getGenes()) {
                Species oldSpeciesOfGenesGene = genesGene.getSpecies();
                genesGene.setSpecies(species);
                genesGene = em.merge(genesGene);
                if (oldSpeciesOfGenesGene != null) {
                    oldSpeciesOfGenesGene.getGenes().remove(genesGene);
                    oldSpeciesOfGenesGene = em.merge(oldSpeciesOfGenesGene);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSpecies(species.getId()) != null) {
                throw new PreexistingEntityException("Species " + species + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(Species species) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Species persistentSpecies = em.find(Species.class, species.getId());
            Set<Gene> genesOld = persistentSpecies.getGenes();
            Set<Gene> genesNew = species.getGenes();
            List<String> illegalOrphanMessages = null;
            for (Gene genesOldGene : genesOld) {
                if (!genesNew.contains(genesOldGene)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Gene " + genesOldGene + " since its species field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Set<Gene> attachedGenesNew = new HashSet<Gene>();
            for (Gene genesNewGeneToAttach : genesNew) {
                genesNewGeneToAttach = em.getReference(genesNewGeneToAttach.getClass(), genesNewGeneToAttach.getId());
                attachedGenesNew.add(genesNewGeneToAttach);
            }
            genesNew = attachedGenesNew;
            species.setGenes(genesNew);
            species = em.merge(species);
            for (Gene genesNewGene : genesNew) {
                if (!genesOld.contains(genesNewGene)) {
                    Species oldSpeciesOfGenesNewGene = genesNewGene.getSpecies();
                    genesNewGene.setSpecies(species);
                    genesNewGene = em.merge(genesNewGene);
                    if (oldSpeciesOfGenesNewGene != null && !oldSpeciesOfGenesNewGene.equals(species)) {
                        oldSpeciesOfGenesNewGene.getGenes().remove(genesNewGene);
                        oldSpeciesOfGenesNewGene = em.merge(oldSpeciesOfGenesNewGene);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = species.getId();
                if (findSpecies(id) == null) {
                    throw new NonexistentEntityException("The species with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
  
            em = getEntityManager();
            em.getTransaction().begin();
            Species species;
            try {
                species = em.getReference(Species.class, id);
                species.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The species with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Set<Gene> genesOrphanCheck = species.getGenes();
            for (Gene genesOrphanCheckGene : genesOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Species (" + species + ") cannot be destroyed since the Gene " + genesOrphanCheckGene + " in its genes field has a non-nullable species field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(species);
            em.getTransaction().commit();
    
    }

    public List<Species> findSpeciesEntities() {
        return findSpeciesEntities(true, -1, -1);
    }

    public List<Species> findSpeciesEntities(int maxResults, int firstResult) {
        return findSpeciesEntities(false, maxResults, firstResult);
    }

    private List<Species> findSpeciesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Species.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public Species findSpecies(Integer id) {
        EntityManager em = getEntityManager();

        return em.find(Species.class, id);

    }

    public int getSpeciesCount() {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Species> rt = cq.from(Species.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

}
