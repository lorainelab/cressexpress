package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.facade.ExperimentFacade;
import com.lorainelab.cressexpress.model.Experiment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.lang.StringUtils;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;

@Stateless
@LocalBean
public class ExperimentController implements Serializable {

    @EJB
    private com.lorainelab.cressexpress.facade.ExperimentFacade ejbFacade;

    public ExperimentController() {
    }

    public ExperimentFacade getEjbFacade() {
        return ejbFacade;
    }

    public List<Experiment> luceneSearch(String queryText) {
        if (StringUtils.isBlank(queryText)) {
            return new ArrayList<>();
        }
        try {
            FullTextEntityManager fullTextEntityManager
                    = org.hibernate.search.jpa.Search.getFullTextEntityManager(ejbFacade.getEntityManager());
            QueryBuilder qb = fullTextEntityManager.getSearchFactory()
                    .buildQueryBuilder().forEntity(Experiment.class).get();
            org.apache.lucene.search.Query luceneQuery = qb
                    .keyword()
                    .onFields("name", "title", "summary", "microarraySet.name", "microarraySet.title", "microarraySet.summary")
                    .matching(queryText)
                    .createQuery();

            javax.persistence.Query jpaQuery
                    = fullTextEntityManager.createFullTextQuery(luceneQuery, Experiment.class);

            List<Experiment> result = (List<Experiment>) jpaQuery.getResultList();
            return result;
        } catch (Throwable t) {
            //TODO add logging here!!!
            return new ArrayList<>();
        }

    }

    public List<Experiment> findExperimentEntities() {
        return findExperimentEntities(true, -1, -1);
    }

    public List<Experiment> findExperimentEntities(int maxResults, int firstResult) {
        return findExperimentEntities(false, maxResults, firstResult);
    }

    private List<Experiment> findExperimentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Experiment.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Experiment findExperiment(Integer id) {
        EntityManager em = ejbFacade.getEntityManager();
        return em.find(Experiment.class, id);
    }

    public int getExperimentCount() {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Experiment> rt = cq.from(Experiment.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
