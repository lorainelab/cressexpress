/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.lorainelab.cressexpress.controller.ExperimentController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@Singleton
public class LuceneIndexManager {

    //TODO if desired in the future this could be externalized to property file, but it is really inexpensive at the moment to fully rebuild
    private static final boolean FULL_REINDEX = true;

    @Inject
    private ExperimentController experimentController;

    @PostConstruct
    public void setup() {
        //Initialize Lucene Index
        if (FULL_REINDEX) {
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(experimentController.getEjbFacade().getEntityManager());
            try {
                fullTextEntityManager.createIndexer().startAndWait();
            } catch (InterruptedException ex) {
                Logger.getLogger(ExperimentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
