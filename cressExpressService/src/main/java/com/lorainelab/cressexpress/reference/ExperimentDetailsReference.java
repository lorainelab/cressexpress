/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.google.common.collect.ImmutableMap;
import com.lorainelab.cressexpress.controller.ExperimentController;
import com.lorainelab.cressexpress.model.Experiment;
import com.lorainelab.cressexpress.model.Microarray;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@DependsOn("ReleaseVersionReference")
@Singleton
public class ExperimentDetailsReference {

    @Inject
    private ExperimentController experimentController;

    @Inject
    private ReleaseVersionReference releaseVersionReference;

    private Map<Experiment, Set<Microarray>> defaultReleaseMicroarrayData;
    
    @PostConstruct
    public void setup() {
        List<String> defaultMicroarrayOrder = releaseVersionReference.getDefaultMicroarrayOrder();
        ImmutableMap.Builder<Experiment, Set<Microarray>> mapBuilder = new ImmutableMap.Builder<>();
        for (Experiment e : experimentController.findExperimentEntities()) {
            Set<Microarray> microarrayList = new HashSet<>();
            for (Microarray microarray : e.getMicroarraySet()) {
                if (defaultMicroarrayOrder.contains(microarray.getName())) {
                    microarrayList.add(microarray);
                }
            }
            mapBuilder.put(e, microarrayList);

        }
        defaultReleaseMicroarrayData = mapBuilder.build();
    }

    public Map<Experiment, Set<Microarray>> getDefaultReleaseMicroarrayData() {
        return defaultReleaseMicroarrayData;
    }

}
