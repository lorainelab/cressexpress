/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "assembly")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Assembly.findAll", query = "SELECT a FROM Assembly a"),
    @NamedQuery(name = "Assembly.findById", query = "SELECT a FROM Assembly a WHERE a.id = :id"),
    @NamedQuery(name = "Assembly.findByDate", query = "SELECT a FROM Assembly a WHERE a.date = :date"),
    @NamedQuery(name = "Assembly.findByName", query = "SELECT a FROM Assembly a WHERE a.name = :name")})
public class Assembly implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "name")
    private String name;
    @JoinColumn(name = "species", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Species species;

    public Assembly() {
    }

    public Assembly(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assembly)) {
            return false;
        }
        Assembly other = (Assembly) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Assembly[ id=" + id + " ]";
    }
    
}
