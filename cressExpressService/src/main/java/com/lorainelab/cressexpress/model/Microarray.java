/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.search.annotations.Field;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "microarray", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"name"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Microarray.findAll", query = "SELECT m FROM Microarray m"),
    @NamedQuery(name = "Microarray.findById", query = "SELECT m FROM Microarray m WHERE m.id = :id"),
    @NamedQuery(name = "Microarray.findByName", query = "SELECT m FROM Microarray m WHERE m.name = :name"),
    @NamedQuery(name = "Microarray.findByTitle", query = "SELECT m FROM Microarray m WHERE m.title = :title"),
    @NamedQuery(name = "Microarray.findBySummary", query = "SELECT m FROM Microarray m WHERE m.summary = :summary")})
public class Microarray implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Field
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
   
    @Field
    @Column(name = "title")
    private String title;
    @Field
    @Column(name = "summary")
    private String summary;
    @JoinTable(name = "experiment2array", joinColumns = {
        @JoinColumn(name = "microarray", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "experiment", referencedColumnName = "id")})
    @ManyToMany
    private Set<Experiment> experimentSet;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "microarray")
    private Set<ExprColumn> exprColumnSet;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "microarray")
    private Set<MicroarrayAttribute> microarrayAttributes;

    public Microarray() {
    }

    public Microarray(Integer id) {
        this.id = id;
    }

    public Microarray(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @XmlTransient
    public Set<Experiment> getExperimentSet() {
        return experimentSet;
    }

    public void setExperimentSet(Set<Experiment> experimentSet) {
        this.experimentSet = experimentSet;
    }

    public Set<MicroarrayAttribute> getMicroarrayAttributes() {
        return microarrayAttributes;
    }

    public void setMicroarrayAttributes(Set<MicroarrayAttribute> microarrayAttributes) {
        this.microarrayAttributes = microarrayAttributes;
    }

    public Set<ExprColumn> getExprColumnSet() {
        return exprColumnSet;
    }

    public void setExprColumnSet(Set<ExprColumn> exprColumnSet) {
        this.exprColumnSet = exprColumnSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Microarray)) {
            return false;
        }
        Microarray other = (Microarray) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Microarray[ id=" + id + " ]";
    }

}
