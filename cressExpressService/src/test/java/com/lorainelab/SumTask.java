/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

import java.util.concurrent.RecursiveTask;

/**
 *
 * @author dcnorris
 */
public class SumTask extends RecursiveTask {
   private final long[] array;
   private final int low;
   private final int high;

   private static final int THRESHOLD = 5000;

   public SumTask(long[] array, int low, int high) {
      super();

      this.array = array;
      this.low = low;
      this.high= high;
   }

   @Override
   protected Long compute() {
      if (high - low < THRESHOLD) {
          long sum = 0;

          for (int i = low; i < high; ++i){
              sum += array[i];
           }

           return sum;
       } else {
           int mid = (low + high) >>> 1;

          RecursiveTask left = new SumTask(array, low, mid);
          RecursiveTask right = new SumTask(array, mid, high);

          right.fork();

          return ((Long)left.getRawResult()) + ((Long)right.join());
      }
   }
}