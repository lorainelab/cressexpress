/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.core.RMap;

/**
 *
 * @author dcnorris
 */
public class RedissonTest {

    @Test
    public void redis() {
        Redisson redisson = Redisson.create();
        RMap<String, String> map = redisson.getMap("anyMap");
        map.fastPut("David", "Norris");
        System.out.println(map.get("David"));
        redisson.shutdown();
    }
}
